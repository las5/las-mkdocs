---
title: Welcome
---

# Lost Alien Society Wiki

## About

Lost Alien Society About Us, Coming Soon...

## Contact

| Team                          | Contact                        | Channel               |
|-------------------------------|--------------------------------|-----------------------|
| Co-Founder/Project Lead       | Nicholas                       |[~twitter](https://twitter.com/NichoLASnft28)|
| Co-Founder/Brand Ambassador   | Shane                          |[~twitter](https://twitter.com/ballardmoney) |
| Co-Founder/Marketer           | AV                             |[~email](mailto:anna@decurian.com)           |
| Community Manager             | Manny                          |[~twitter](https://twitter.com/berryislive)  |
| Art                           | Sherry                         |[~contact]                                   |
| Design                        | Sapkernel                      |[~development](https://twitter.com/sapkernel)|
| Development                   | Sapkernel                      |[~development](https://twitter.com/sapkernel)|
| Documentation                 | Sapkernel                      |[~documentation](https://docs.lostaliensociety.com)   |

For all other questions: contact@lostaliensociety.com


## Contributing

Thank you for your interest in contributing to the project.

If you are a developer, architect, engineer, or otherwise looking to contribute your time and expertise, please consider joining our [Discord Mattermost server](https://discord.lostaliensociety.com) and jumping into the most relevant channel to your interests.


## Special Thanks

We love our Alliens!
