FROM ubuntu:latest

RUN apt update -y &&\
    apt install -y git python3 &&\
    apt-get -y install python3-pip &&\
    apt clean all

RUN pip install --upgrade pip

# bind volume $CWD > /wiki exists in docker-compose.yml
RUN mkdir /wiki
COPY requirements.txt /wiki
WORKDIR /wiki
RUN pip install -r requirements.txt

EXPOSE 80
CMD mkdocs serve -a 0.0.0.0:80
